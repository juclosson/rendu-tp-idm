#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <ctime>
#include <string>
#include "Random/CLHEP/Random/MTwistEngine.h"


/*********************************************************/
/* N simulation de Pi avec 1 milliard de point           */
/* en utilisant une seed différente à chaque             */
/*********************************************************/
int main()
{
    int tirage = 1000000000;  //nb de point à tirer

    CLHEP::MTwistEngine * mtRng = new CLHEP::MTwistEngine();

    int i;
    int j;
    float x_alea;   //Abscisse du point 
    float y_alea;   //Ordonnée du point 

    time_t debut, fin;

    std::string nom_fichier_base = "q3_status";
    std::string nom_fichier = "";

    time(&debut);

  
    for (i = 0; i < 10; i++)
    {
        //simulation de PI
        double pi = 0;

        nom_fichier = nom_fichier_base + std::to_string(i);
        mtRng->restoreStatus(nom_fichier.c_str());

        for (j = 0; j < tirage; j++)
        {
            x_alea = (mtRng->flat() - 0.5) * 2;  
            y_alea = (mtRng->flat() - 0.5) * 2; 
            
            
            //on regarde si le point est dans le cercle de rayon 1
            if ( x_alea*x_alea + y_alea*y_alea <= 1)
            {
                pi += 1;
            }
        }

        pi /= tirage;   //proportion de l'aire du cercle par rapport à
                        //celle du carré
        pi *= 4;        //Multiplié par l'aire du carré, donne l'aire du 
                        //cercle, soit pi
        
        std::cout << "pi = " << pi << std::endl;
    }
    
    time(&fin);

    std::cout << "temps d'exécution : " << difftime(fin, debut) <<  " s"<< std::endl;

    return 0;
}