#include <iostream>
#include <ctime>
#include <string>
#include <thread>
#include "CLHEP/Random/MTwistEngine.h"

/***********************/
/*Calcul de Pi une fois*/
/***********************/
void calculerPI(int replicationNumber) {
    std::string nom_fichier_base = "q3_status"; 
    int tirage = 1000000000;  //nb de points à tirer
    CLHEP::MTwistEngine * mtRng = new CLHEP::MTwistEngine();

    int i;
    int j;
    float x_alea;   // Abscisse du point 
    float y_alea;   // Ordonnée du point 

    // Simulation de PI
    double pi = 0;

    std::string nom_fichier = nom_fichier_base + std::to_string(replicationNumber);
    mtRng->restoreStatus(nom_fichier.c_str());

    for (j = 0; j < tirage; j++) {
        x_alea = (mtRng->flat() - 0.5) * 2;  
        y_alea = (mtRng->flat() - 0.5) * 2; 
            
        // On regarde si le point est dans le cercle de rayon 1
        if (x_alea*x_alea + y_alea*y_alea <= 1) 
        {
            pi += 1;
        }
    }

    pi /= tirage;   // Proportion de l'aire du cercle par rapport à celle du carré
    pi *= 4;        // Multiplié par l'aire du carré, donne l'aire du cercle, soit pi
        
    std::cout << "pi = " << pi << std::endl;
    

    delete mtRng;  //  libéreration de  la mémoire
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <replication-number>" << std::endl;
        return 1;
    }

    int replicationNumber = std::stoi(argv[1]);  // Convertit le numéro de réplication en entier
    time_t debut, fin;

    time(&debut);

    // Créer 10 threads pour effectuer les réplications en parallèle
    std::thread threads[replicationNumber];
    for (int i = 0; i < replicationNumber; i++) {
        threads[i] = std::thread(calculerPI, i);
    }

    // Attendre que tous les threads se terminent
    for (int i = 0; i < replicationNumber; i++) {
        threads[i].join();
    }

    time(&fin);

    std::cout << "temps d'exécution : " << difftime(fin, debut) << " s" << std::endl;

    return 0;
}