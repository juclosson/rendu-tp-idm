# TP IDM

Afin de compiler les fichiers :
```
cd Random
```

Pour la question 2 :
```
g++ ../q2.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o q2
```

Pour la question 3 :
```
g++ ../q3.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o q3
```
(aucun résultat visble car ne fait qu'enregistrer le Etats pour les questions suivantes)


Pour la question 4 :
```
g++ ../q4.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o q4
```

Pour la question 5 :
```
g++ ../q5.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o q5
```
/!\ à l'execution de la question 5 penser à mettre un nombre de thread en parallele voulu, ex: ./q5 10
