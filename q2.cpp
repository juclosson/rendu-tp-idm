#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include "Random/CLHEP/Random/MTwistEngine.h"

/*********************************************************/
/* Simple test des fonctions saveStatus et RestorStatus  */
/* donc nombre de tirage limité                          */
/*********************************************************/
int main()
{
    CLHEP::MTwistEngine * mtRng = new CLHEP::MTwistEngine();

    int i;
    int j;
    double fRn;
    unsigned int iRn;
    std::string nom_fichier_base = "q2_status";
    std::string nom_fichier = "";

    for (i = 0; i < 5; i++)
    {
        //enregistrement de l'état actuel
        nom_fichier = nom_fichier_base + std::to_string(i);
        mtRng->saveStatus(nom_fichier.c_str());

        std::cout << "\nétat: i = " << i << std::endl;
        
        for (j = 0; j < 10; j++)
        {
            fRn = mtRng->flat();   
            iRn = (unsigned int) (fRn * UINT_MAX);
            std::cout << iRn << std::endl;
        }

    }

    //on essais de restaurer les séris enregistrés au préalable
    std::cout << "\nRestauration à partir de l'état 1 \n" << std::endl;
   

    mtRng->restoreStatus("q2_status1");

    for (j = 0; j < 10; j++)
    {
        fRn = mtRng->flat();    
        iRn = (unsigned int) (fRn * UINT_MAX);
        std::cout << iRn << std::endl;
    }

    return 0;
}